Placeholder issue for availability highlights.

Please follow the [Availability Highlights process](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/26032#note_2283210684) to generate the content for this issue.

/label ~"AvailabilityHighlights"
/label ~"workflow-infra::In Progress"
/confidential
/assign me

<!-- script for generating the report lives in https://gitlab.com/gitlab-com/gl-infra/reliability-reports -->
