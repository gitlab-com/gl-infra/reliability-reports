#!/usr/bin/env ruby

require 'gitlab'
require 'date'
require 'http'
require 'active_support/all'
require 'concurrent-ruby'
require 'curb'

# export GITLAB_RO_API_TOKEN=
# export PAGERDUTY_TOKEN=

# ssh -D 18200 bastion-01-inf-ops.c.gitlab-ops.internal
#
# export VAULT_ADDR=https://vault.ops.gke.gitlab.net
# export VAULT_PROXY_ADDR=socks5://localhost:18200
#
# glsh vault login
#
# export PROMETHEUS_USERNAME=$(vault kv get -field=username -mount="k8s" "shared/observability/tenants/runbooks")
# export PROMETHEUS_PASSWORD=$(vault kv get -field=password -mount="k8s" "shared/observability/tenants/runbooks")
# export PROMETHEUS_QUERY_URL="https://mimir-internal.ops.gke.gitlab.net/prometheus"
# export PROMETHEUS_PROXY_ADDR=socks5://localhost:18200

client = Gitlab.client(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV['GITLAB_RO_API_TOKEN']
)

ENV['TZ'] = 'UTC'

if raw_date = ARGV.shift
  start_date = DateTime.parse(raw_date)
else
  start_date = DateTime.parse(DateTime.now.strftime("%Y-%m-01T00:00:00%z")).prev_month
end

end_date = start_date.next_month

project = 'gitlab-com/gl-infra/production'

gl_incidents = []
pd_incidents = []

$gl_users = [] # { name: ..., email: ..., gluser: ... , glid: ...}

def lookup_gl_user(id, client)
  entry = $gl_users.find { |i| i[:glid] == id }
  return entry if entry

  u = client.user(id)
  record = {
    name: u.name,
    email: u.public_email,
    glusername: u.username,
    glid: u.id
  }
  $gl_users << record
  record
end

def lookup_pd_user(id, name)
  entry = $gl_users.find { |i| i[:name] == name || i[:pd_name] == name }
  return entry if entry

  user = JSON.parse(HTTP.headers(accept: 'application/vnd.pagerduty+json;version=2').headers(authorization: "Token token=#{ENV['PAGERDUTY_TOKEN']}").get("https://api.pagerduty.com/users/#{id}"))
  name = user['user']['name']
  email = user['user']['email']

  entry = $gl_users.find { |i| i[:email] == email }
  if entry
    entry[:pd_name] = name
    return entry
  end

  record = {
    pd_name: name,
    email: email
  }
  $gl_users << record
  record
end

def severity_stat_section(gl_incidents)
  s = []
  tallies = gl_incidents.map { |i| i[:severity].gsub('severity::', 'Sev') }.tally
  tallies.sort.to_h.each do |sev, count|
    s << "section #{sev}"
    s << "#{count} : 0,#{count}"
  end
  s
end

def pd_stat_section(user_counts)
  s = []
  count = 1
  user_counts.sort_by { |_k, v| v['pd_incidents'] }.reverse.slice(0, 5).to_h.each do |name, tally|
    s << "section #{count} 👤 #{name.scan(/([[:alpha:]])[[:alpha:]]*/).flatten.join.upcase}"
    s << "#{name} (#{tally['pd_incidents']} PD pages) : 0,#{tally['pd_incidents']}"
    s << "#{name} (#{tally['gl_incidents']} declared incidents) : 0,#{tally['gl_incidents']}"
    count += 1
  end
  s
end

def ca_info(client, project_id, iid)
  links = []
  client.issue_links(project_id, iid).each_page do |p|
    p.each do |i|
      ['corrective action', 'infradev'].each do |l|
        state = i.state == 'closed' ? ' (closed)' : ''
        links.append("[#{l}](#{i.web_url})#{state}") if i.labels.include?(l)
      end
    end
  end
  links.empty? ? ':x:' : links.join(' / ')
end

def review_info(client, project_id, iid)
  client.issue_links(project_id, iid).each_page do |p|
    p.each do |i|
      next unless i.labels.include?('incident-review')

      assigned = i&.assignee&.username
      state = i.state == 'closed' ? ' (closed)' : ''
      return "#{assigned ? "`@#{assigned}`" : '**unassigned**'} / [link](#{i.web_url})#{state}"
    end
  end
  ':x:'
end

def prometheus_sla_for_day(time)
  query = 'clamp_min(clamp_max(avg_over_time(sla:gitlab:ratio{env=~"ops|gprd",environment="gprd",monitor=~"global|",sla_type="weighted_v2.1",stage="main"}[1d]),1),0)'

  url = ENV['PROMETHEUS_QUERY_URL'] + '/api/v1/query'

  c = Curl::Easy.new(Curl::urlalize(url, query: query, time: time.strftime('%Y-%m-%dT%H:%M:%SZ'))) do |c|
    # c.verbose = true
    c.proxy_tunnel = true
    c.proxy_url = ENV['PROMETHEUS_PROXY_ADDR']

    c.headers['X-Scope-OrgID'] = 'gitlab-gprd'

    c.http_auth_types = :basic
    c.username = ENV['PROMETHEUS_USERNAME']
    c.password = ENV['PROMETHEUS_PASSWORD']
  end

  c.http_post

  # warn [time, c.body_str].inspect
  body = JSON.parse(c.body_str)

  if body['data']['result'].size > 0
    body['data']['result'][0]['value'][1].to_f
  else
    nil
  end
end

client.issues(project,
              created_after: start_date.iso8601,
              created_before: end_date.iso8601,
              labels: 'incident',
              per_page: 100).each_page do |issue_page|
  warn '--- Fetching page of GL incidents'
  issue_page.each do |i|
    backstage = i.labels.include?('backstage') ? 'internal' : 'external'
    severity = i.labels.filter { |l| l.start_with?('severity') }
    root_cause = i.labels.filter { |l| l.start_with?('RootCause') }
    service = i.labels.filter { |l| l.start_with?('Service') }
    next if severity.length != 1

    gl_incidents.append(
      {
        created_at: i.created_at,
        title: i.title,
        web_url: i.web_url,
        backstage: backstage,
        severity: severity.first,
        root_cause: root_cause.first,
        service: service.first,
        status_page?: i.labels.any? { |l| l.start_with?('Incident-Comms') },
        backstage?: i.labels.include?('backstage'),
        corrective_actions_needed?: i.labels.include?('CorrectiveActions::Needed'),
        incident_review_needed?: i.labels.include?('IncidentReview::Needed'),
        disp_labels: severity + i.labels.filter { |l| l.start_with?('Incident-Comms') } + i.labels.filter { |l| l == 'backstage' },
        users: i.assignees.map { |a| lookup_gl_user(a.id, client) },
        project_id: i.project_id,
        iid: i.iid,
        id: i.id
      }
    )
  end
end

offset = 0
loop do
  warn '--- Fetching page of PD incidents'
  params = {
    since: start_date.iso8601,
    until: end_date.iso8601,
    include: %w[agents teams users],
    service_ids: %w[PFQXAWL PATDFCE PZ6SJXH P7Q44DU P3DG414],
    limit: 100,
    total: true,
    offset: offset
  }.to_query
  incidents = JSON.parse(HTTP.headers(accept: 'application/vnd.pagerduty+json;version=2')
                .headers(authorization: "Token token=#{ENV['PAGERDUTY_TOKEN']}")
                .get('https://api.pagerduty.com/incidents?' + params))
  incident_logs = Concurrent::Map.new

  # PD api rate limit is 960 req/min, so roughly 16 QPS if sustained
  incidents['incidents'].each do |i|
    incident_logs[i['id']] = Concurrent::Promise.execute {
      JSON.parse(HTTP.headers(accept: 'application/vnd.pagerduty+json;version=2').headers(authorization: "Token token=#{ENV['PAGERDUTY_TOKEN']}").get("https://api.pagerduty.com/incidents/#{i['id']}/log_entries"))
    }
  end

  incidents['incidents'].each do |i|
    log = incident_logs[i['id']].value!
    entry = log['log_entries'].find { |e| e['type'] == 'notify_log_entry' } || log['log_entries'].find { |e| e['type'] == 'resolve_log_entry' }
    unless entry
      warn log
      raise
    end

    user_entry = entry['user'] || entry['agent']
    pd_incidents.append(
      {
        created_at: i['created_at'],
        status: i['status'],
        title: i['title'],
        web_url: i['html_url'],
        users: [lookup_pd_user(user_entry['id'], user_entry['summary'])]
      }
    )
  end
  break unless incidents['more']

  offset += incidents['incidents'].length
end

user_counts = {}
pd_incidents.each do |p|
  p[:users].each do |u|
    name = u[:pd_name] || u[:name]
    user_counts[name] ||= (Hash.new 0)
    user_counts[name]['pd_incidents'] += 1
  end
end

gl_incidents.each do |g|
  g[:users].each do |u|
    name = u[:pd_name] || u[:name]
    user_counts[name] ||= (Hash.new 0)
    user_counts[name]['gl_incidents'] += 1
  end
end

dates = (start_date...end_date)

warn '--- Fetching SLAs from prometheus'
slas = dates
  # .map { |date| [date, prometheus_sla_for_day(date+1)] }
  .map { |date| [date, Concurrent::Promise.execute { prometheus_sla_for_day(date+1) }] }
  .map { |date, p| [date, p.value!] }
  .to_h

availability_url_params = {
  orgId: 1,
  from: start_date.strftime("%Y-%m-%dT00:00:00%z"),
  to: end_date.strftime("%Y-%m-%dT00:00:00%z"),
  timezone: 'utc',
  'var-PROMETHEUS_DS': 'default',
  'var-environment': 'gprd',
  'var-sla_type': 'weighted_v2.1',
  'var-web_weight': '5',
  'var-git_weight': '5',
  'var-api_weight': '5',
  'var-ci_runners_weight': '0',
  'var-registry_weight': '1',
  'var-web_pages_weight': '0',
  'var-sidekiq_weight': '0',
  'var-internal_api_weight': '5',
}

availability_base_url = 'https://dashboards.gitlab.net/d/general-slas/general3a-slas'
availability_url = availability_base_url + '?' + URI.encode_www_form(availability_url_params)
availability_with_sidekiq_url = availability_base_url + '?' + URI.encode_www_form(availability_url_params.merge({
  'var-sla_type': 'weighted_v3',
  'var-internal_api_weight': '0',
}))
availability_with_sidekiq_pages_runners_url = availability_base_url + '?' + URI.encode_www_form(availability_url_params.merge({
  'var-web_pages_weight': '1',
  'var-sidekiq_weight': '1',
  'var-internal_api_weight': '5',
}))

puts <<~HEADER
  **Availability**:

  * [{+xxx%+}](#{availability_url})
  * [{+xxx%+}](#{availability_with_sidekiq_url}) (with sidekiq)
  * [{+xxx%+}](#{availability_with_sidekiq_pages_runners_url}) (with sidekiq, pages, runners)

  - **Sidekiq Availability**: {+xxx%+}
  - **Runner Availability**: {+xxx%+}
  - **Pages Availability**: {+xxx%+}
HEADER

puts <<~CHARTS
  ## :information_source: Notes about availability this month

  xxx

  * :frowning2: **x** status page events compared to **x** last month
  * :neutral_face: **x** user-facing high severity incidents compared to **x** last month
  * :neutral_face:  **x** declared incidents compared to **x** last month
  * :frowning2:  **x** pages compared to **x** last month

  ## :fire: High Severity and Status Page User-Impacting Issues

  * x was due to saturation, traffic to a single repository
  * x was due to saturation, emails being rejected that was resolved by our vendor (mailgun)
  * x was due to software change
  * x was due to a software change behind a feature flag.

  | Incident | CA/Infradev? | Review? |
  | --- | --- | --- |
CHARTS

promises = gl_incidents.filter { |i| !i[:backstage?] && (i[:status_page?] || %w[severity::1 severity::2].include?(i[:severity])) }.map do |i|
  Concurrent::Promise.execute {
    "| [#{i[:title].gsub(' | ', '')}](#{i[:web_url]}) #{i[:disp_labels].map { |l| "~\"#{l}\"" }.join(' ')} " \
      "| #{i[:corrective_actions_needed] ? ':x:' : ca_info(client, i[:project_id], i[:iid])} " \
      "| #{i[:incident_review_needed] ? ':x:' : review_info(client, i[:project_id], i[:iid])} " \
      '|'
  }
end

promises.each do |p|
  puts p.value!
end

puts <<~CHARTS
  ## Incident Issue Stats

  ```mermaid
  gantt
      title Incident Severities
      dateFormat  X
      axisFormat %s

      #{severity_stat_section(gl_incidents).join("\n    ")}
  ```

CHARTS

puts <<~END_TALLY

  ## User Tallies

  ```mermaid
  gantt
      title Most pages this month (top 5)
      dateFormat  X
      axisFormat %s

      #{pd_stat_section(user_counts).join("\n    ")}
  ```
END_TALLY

panes_data = {
  mos: {
    datasource: 'e58c2f51-20f8-4f4b-ad48-2968782ca7d6',
    queries: [
      {
        expr: 'clamp_min(clamp_max(avg_over_time(sla:gitlab:ratio{env=~"ops|gprd",environment="gprd",monitor=~"global|",sla_type="weighted_v2.1",stage="main"}[1d]),1),0)',
        format: 'time_series',
        interval: '',
        intervalFactor: 3,
        refId: 'A',
        datasource: {type: 'prometheus', uid: 'e58c2f51-20f8-4f4b-ad48-2968782ca7d6'},
        editorMode: 'code',
        range: true,
        instant: true,
      }
    ],
    range: {
      from: start_date.strftime("%Y-%m-%dT00:00:00%z"),
      to: end_date.strftime("%Y-%m-%dT00:00:00%z"),
    }
  }
}

availability_drops_base_url = 'https://dashboards.gitlab.net/explore'
availability_drops_url = availability_drops_base_url + '?' + URI.encode_www_form({
  orgId: '1',
  schemaVersion: '1',
  panes: panes_data.to_json,
})

puts <<~END_HEADER
  ## Incident and Page Report

  <details>

  * :notebook: [Availability drops](#{availability_drops_url})

  | Day | Incident Issue (#{gl_incidents.length})| PD Page (#{pd_incidents.length}) |
  | --- | --- | --- |
END_HEADER

dates.each do |date|
  sla = slas[date]

  gl = gl_incidents.filter { |i| Time.parse(i[:created_at]) >= date && Time.parse(i[:created_at]) < date + 1 }.sort_by { |k| k[:created_at] }
  pd = pd_incidents.filter { |i| Time.parse(i[:created_at]) >= date && Time.parse(i[:created_at]) < date + 1 }.sort_by { |k| k[:created_at] }

  availability_single_day_url = availability_base_url + '?' + URI.encode_www_form(availability_url_params.merge({
    from: date.strftime("%Y-%m-%dT00:00:00%z"),
    to: (date + 1).strftime("%Y-%m-%dT00:00:00%z"),
  }))

  sla_html = ''
  if sla
    if sla >= 0.9995
      sla_html = "[{+#{format('%.2f', sla*100)}%+}](#{availability_single_day_url})"
    else
      sla_html = "[{-#{format('%.2f', sla*100)}%-}](#{availability_single_day_url})"
    end
  end

  gl_html = ''
  gl.each do |i|
    gl_html += "#{Time.parse(i[:created_at]).strftime('%H:%M')} [#{i[:title].gsub(' | ', '')}](#{i[:web_url]}) #{i[:disp_labels].map { |l| "~\"#{l}\"" }.join(' ')}<br>"
  end
  gl_html += '<br>'
  gl_html += gl.map { |i| i[:users] }.flatten.map { |u| u[:pd_name] || u[:name] }.group_by(&:to_s).map { |a| [a[0], a[1].count] }.map { |i| "_#{i[0]} (#{i[1]})_" }.join(' / ')

  pd_html = ''
  pd.each do |i|
    pd_html += "#{Time.parse(i[:created_at]).strftime('%H:%M')} [#{i[:title].gsub(' | ', '')}](#{i[:web_url]})<br>"
  end
  pd_html += '<br>'
  pd_html += pd.map { |i| i[:users] }.flatten.map { |u| u[:pd_name] || u[:name] }.group_by(&:to_s).map { |a| [a[0], a[1].count] }.map { |i| "_#{i[0]} (#{i[1]})_" }.join(' / ')

  day = date.strftime('%m-%d')

  puts "| **#{day}**<br>#{sla_html} | #{gl_html} | #{pd_html} |" unless pd.empty? && gl.empty? && (sla.nil? || sla == 1.0)
end

puts <<~END_FOOTER

  </details>

  ---

  _Generated via [reliability-reports](https://gitlab.com/gitlab-com/gl-infra/reliability-reports)._
END_FOOTER
